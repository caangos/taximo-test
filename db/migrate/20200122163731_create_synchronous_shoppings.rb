class CreateSynchronousShoppings < ActiveRecord::Migration[5.2]
  def change
    create_table :synchronous_shoppings do |t|
      t.text :parameters
      t.text :shopping_centers
      t.text :roads
      t.string :minimum_time
      t.string :username

      t.timestamps
    end
  end
end
