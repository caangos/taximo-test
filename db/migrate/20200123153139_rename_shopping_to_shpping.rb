class RenameShoppingToShpping < ActiveRecord::Migration[5.2]
  def change
    rename_column :synchronous_shoppings, :shopping_centers, :shoping_centers
  end
end
