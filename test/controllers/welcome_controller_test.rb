require 'test_helper'

class WelcomeControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get welcome_index_url
    assert_response :success
    assert_select "body", "Taximo Test developed by Carlos A. Arboleda :)"
  end

end
