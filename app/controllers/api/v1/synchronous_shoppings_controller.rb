class Api::V1::SynchronousShoppingsController < ApplicationController
  before_action :authorize_user, only: [:create]

  # GET /synchronous_shoppings
  def index
    # List data from data base
    @synchronous_shoppings = SynchronousShopping.where.not(roads: nil)
    render json: @synchronous_shoppings
  end

  # POST /synchronous_shoppings
  def create
    # Calling lib with synchronous_shopping logic and calculate
    result = SynchronousShoppingLogic.new(params).minimum_time

    # Only trsted params
    if params[:info].blank?
      @synchronous_shopping = SynchronousShopping.new(synchronous_shopping_params.merge(minimum_time: result))
    else
      @synchronous_shopping = SynchronousShopping.new(synchronous_shopping_params.merge(minimum_time: result, parameters: params[:info]))
    end

    # Validating saving
    if @synchronous_shopping.save
      render json: { minimum_time: result }
    else
      render json: @synchronous_shopping.errors, status: :unprocessable_entity
    end
  end

  private

  # Only allow a trusted parameter "white list" through
  def synchronous_shopping_params
    params.permit(:parameters, :shoping_centers, :roads, :username)
  end
end
