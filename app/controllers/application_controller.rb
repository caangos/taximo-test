class ApplicationController < ActionController::Base
  skip_before_action :verify_authenticity_token

  # Method to ckeck user authorization
  def authorize_user
    UserAccess.check_user(params[:username], params[:checksum])
  rescue StandardError => e
    render json: { errors: e }, status: :unauthorized
  end
end
