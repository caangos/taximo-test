class HomeController < ApplicationController
  def index
    @synchronous_shoppings = SynchronousShopping.where(roads: nil)
  end

  def new
  end
end
