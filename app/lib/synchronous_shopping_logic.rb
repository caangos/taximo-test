class SynchronousShoppingLogic

  attr_reader :minimum_time

  def initialize(params)
    # Parsing params from POST request
    intilize_parameters(params)

    # Initialize some variables to limit the params
    max_shop = 1 << 5
    max_fish = 1000

    # Create arrays with the limits
    @shops = Array.new(max_shop, 0)
    @road = Array.new(max_shop) {[]}
    @distance = Array.new(max_shop) { Array.new(max_fish) {Float::INFINITY} } # with the to hashes shop and fish to compare
    # Calling methods
    start
    dijk # Graphs methods
    min_cost
  end

  # Method orginize the params in variables
  def start
    @total_shoping_centers, total_roads, @total_fish = @parameters
    put_fish_shop
    get_road(total_roads)
    @minimum_time = Float::INFINITY
    init_travel
  end

  # Method to create shops with the fishes
  def put_fish_shop
    (0..@total_shoping_centers).each do |i|
      @shops[i + 1] = sum(@shoping_centers[i].strip.split.map(&:to_i).drop(1)) # Bits sum
      break if i >= @total_shoping_centers -1
    end
  end

  def get_road(total_roads)
    @roads.in_groups_of(3, false).each_with_index do |road, index|
      shop_x, shop_y, distance = road
      @road[shop_x].push([shop_y, distance])
       @road[shop_y].push([shop_x, distance])
      break if index >= total_roads
    end
  end

  # Method to create routes to travel
  def init_travel
    @distance[1][@shops[1]] = 0
    @queue = Set.new [[1, @shops[1], @distance[1][@shops[1]]]]
  end

  def min_cost
    all_types = (1 << @total_fish) - 1
    (0..all_types).each do |little_cat_collected|
      (0..all_types).each do |big_cat_collected|
        if big_cat_collected | little_cat_collected == all_types
          # get distances infinity array
          cost = [@distance[@total_shoping_centers][little_cat_collected],
                  @distance[@total_shoping_centers][big_cat_collected]].max
          @minimum_time = [@minimum_time, cost].min
        end
      end
    end
  end

  # Graphs methods
  def dijk
    until @queue.empty?
      last_travel = @queue.first
      @queue.delete last_travel
      dijk_neighbour(last_travel)
    end
  end

  def dijk_neighbour(last_travel)
    last_shop, last_collected, distance_to_last_shop = last_travel
    @road[last_shop].each do |current_shop|
      current_shop_number, distance_from_last_shop = current_shop
      collected = last_collected | @shops[current_shop_number]
      distance_to_current_shop = distance_to_last_shop + distance_from_last_shop
      dijk_queue(current_shop_number, distance_to_current_shop, collected)
    end
  end

  def dijk_queue(current_shop_number, distance_to_current_shop, collected)
    travelled_distance = @distance[current_shop_number][collected]
    if distance_to_current_shop < travelled_distance
      @queue.delete [current_shop_number, collected, travelled_distance]
      @distance[current_shop_number][collected] = distance_to_current_shop
      @queue << [current_shop_number, collected,
                 @distance[current_shop_number][collected]]
    end
  end

  private

  def sum(types)
    types.reduce(0) { |sum, num| sum | 1 << (num - 1) }
  end

  # Private method to check if api request or interface
  def intilize_parameters(params)
    if params[:info].blank?
      @parameters = params[:parameters].strip.split(/[\s,'-']/).map(&:to_i)
      @shoping_centers = shoping_centers_api(params[:shoping_centers])
      @roads = params[:roads].strip.split(/[\s,-]/).map(&:to_i)
    else
      @parameters = parameters_interface(params[:info].strip.split.map(&:to_i))
      @shoping_centers = shoping_centers_interface(params[:info])
      @roads = roads_interface( params[:info].strip.split.map(&:to_i))


    end
  end

  protected

  def parameters_interface(info)
    info.slice(0, 3)
  end

  # This treatment is only for api shopping
  def shoping_centers_api(info)
     info.gsub(/,/, ' ').split(/-/)
  end

  # This treatment is only for interface shopping
  def shoping_centers_interface(info)
    list = info.split(/\n/)
    list.slice(1, info.strip.split.map(&:to_i)[0])
  end

  def roads_interface(info)
    num =  info.size - info[1]*3
    info.slice(num, info.size)
  end
end
