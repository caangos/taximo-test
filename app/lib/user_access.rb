class UserAccess
  # Method to validate user access
  def self.check_user(user, check_sum)
    # Convert user to SHA 256
    sha = Digest::SHA256.hexdigest user
    raise 'User Unauthorized' if sha != check_sum # Validate user == params check_sum
  end
end