function synchronous_shopping() {

    if ($('#min_road_text_area').val() === "" || $('#min_road_text_area').val().length < 5) {

        alert("Por favor ingresar los valores");
    } else {

        $.ajax({
            type: "POST",
            url: "/api/v1/synchronous_shopping",
            data: {
                info: $('#min_road_text_area').val(),
                username: "taximo_api_user",
                checksum: "cd7ced88fb72ee862940b1064555251f9ba044d8478a71a7b70b04bd708c2796"
            },
            success: function (data) {
                $('.min_time h2').text('Answer');
                $('.min_time h3').text(data['minimum_time']).addClass('p-3 mb-2 bg-success text-white');
            }, error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        })

    }
}