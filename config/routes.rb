Rails.application.routes.draw do
  get 'welcome/index'
  root 'welcome#index'

  resources :home , path: "synchronous_shopping"

  # Routes API
  namespace :api do
    namespace :v1 do
      resources :synchronous_shoppings, path: "synchronous_shopping"
    end
  end
end
